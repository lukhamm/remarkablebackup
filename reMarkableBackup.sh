#!/bin/bash

# This code is shared under the CC BY-SA 4.0 license.
# A copy of the license can be found here:
# https://creativecommons.org/licenses/by-sa/4.0/
# https://creativecommons.org/licenses/by-sa/4.0/legalcode
#
# Original code taken from:
# https://remarkablewiki.com/tech/ssh
# (Last modified: 2021/03/17 14:40 by mjervis)
# and here
# https://eeems.website/toltec/#2-backup
# (Nathaniel van Diepen)
# Additions are published under the same license as the original.
# No changes were made.

############# CHANGE THIS AS REQUIRED ##################
backup_folder_files="${HOME}/Remarkable_Backup/files/"
backup_folder="${HOME}/Remarkable_Backup/"
########################################################

halt_on_failure () {
	if [[ $i != 0 ]]; then
		exit 1
	fi
}

echo "Starting the backup process"
echo "[1/3] Backing up files..."
# all your content (this will likely take the longest, and could be up to 8GB of data)
scp -r root@remarkable:~/.local/share/remarkable/xochitl/ "$backup_folder_files"
halt_on_failure $?
echo "...done."

echo "[2/3] Backing up config file..."
# your configuration file, which also contains your ssh password
scp root@remarkable:~/.config/remarkable/xochitl.conf "$backup_folder"
halt_on_failure $?
echo "...done."

echo "[3/3] Backing up xochitl binary file..."
# the xochitl binary, if you plan on replacing or modifying it in any way
scp root@remarkable:/usr/bin/xochitl "$backup_folder"
halt_on_failure $?
echo "...done."
