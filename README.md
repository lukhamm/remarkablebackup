# reMarkableBackup

Backup shell script for ubuntu/debian linux

# Warranty

This piece of code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

# License

This code is shared under the CC BY-SA 4.0 license.

A copy of the license can be found here:  
https://creativecommons.org/licenses/by-sa/4.0/  
https://creativecommons.org/licenses/by-sa/4.0/legalcode
